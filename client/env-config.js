const prod = process.env.NODE_ENV === 'production';

module.exports = {
  'process.env.BACKEND_URL': prod ? 'https://m8qmxdz2w0.execute-api.us-east-1.amazonaws.com/production' : 'http://localhost:3000'
};

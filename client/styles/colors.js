const colors = (theme) => ({
  disabled: {
    color: theme.palette.disabled,
  }
});

export default colors;

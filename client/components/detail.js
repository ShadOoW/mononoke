import React from 'react';

import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Player from '../components/player';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar/Avatar';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: '840px',
  },

  disabledText: {
    color: theme.palette.text.disabled,
  },

  container: {
    padding: '0 5px'
  },
});

class Detail extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    const {classes} = this.props;

    return (
        <Grid
            container
            direction="column"
            alignItems="center"
            justify="center"
        >
          <Grid
              container
              justify="center"
              className={classes.root}
          >
            <Player
                videoId={this.props.video.videoId}
            />
            <div className={classes.container}>
              <Grid
                  direction="row"
                  container
                  alignItems="center"
                  wrap="nowrap"
                  className={classes.container}
              >
                <Avatar
                    size={30}
                    alt="Remy Sharp"
                    src={this.props.video.channelAvatar}
                />
                <Typography variant="h5">
                  {this.props.video.title}
                </Typography>
              </Grid>

              <Grid
                  container
                  direction="row"
                  className={classes.disabledText}
              >
                {/*<div className="counter">*/}
                  {/*<Views count={this.props.video.viewCount} />*/}
                {/*</div>*/}

                {/*<div className="counter">*/}
                  {/*<Like count={this.props.video.likeCount} />*/}
                {/*</div>*/}

                {/*<div className="counter">*/}
                  {/*<Dislike count={this.props.video.dislikeCount} />*/}
                {/*</div>*/}
              </Grid>
            </div>
          </Grid>
        </Grid>
    );
  }
}

Detail.propTypes = {
  classes: PropTypes.object.isRequired,
  video: PropTypes.shape({
    id: PropTypes.string.isRequired,
    videoId: PropTypes.string.isRequired,
    channelAvatar: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    viewCount: PropTypes.number.isRequired,
    likeCount: PropTypes.number.isRequired,
    dislikeCount: PropTypes.number.isRequired,
  }).isRequired,
};

export default withStyles(styles)(Detail);

import PropTypes from 'prop-types';
import React from 'react';
import {withRouter} from 'next/router';
import {withTheme} from '@material-ui/core/styles';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';

import Sentiment from './sentiment';

const Video = (props) => {
  return (
      <div>
        {props.video ? (
            <Card>
              <a href={`/?id=${props.video.videoId}`}>
                <CardMedia
                    style={{height: 0, paddingTop: '56.25%'}}
                    image={props.video.thumbnail}
                />
              </a>
              <a href={`/?id=${props.video.videoId}`}>
                <CardContent>
                  <Typography className="truncate" gutterBottom component="h2">
                    {props.video.title}
                  </Typography>
                </CardContent>
              </a>
              <CardActions>
                <div className="flex width-100">
                  <div className="flex flex-1 align-items-center">
                    <Avatar size={30} alt="Remy Sharp" src={props.video.channelAvatar} />

                    <Typography className="padding-left-x1" component="h2">
                      Hesspress
                    </Typography>
                  </div>

                  <Sentiment
                      views={props.video.viewCount}
                      likes={props.video.likeCount}
                      dislikes={props.video.dislikeCount}
                  />
                </div>
              </CardActions>
            </Card>
        ) : null}
      </div>
  );
};

Video.propTypes = {
  theme: PropTypes.object.isRequired,
  video: PropTypes.shape({
    id: PropTypes.string.isRequired,
    videoId: PropTypes.string.isRequired,
    channelAvatar: PropTypes.string.isRequired,
    thumbnail: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    viewCount: PropTypes.number.isRequired,
    likeCount: PropTypes.number.isRequired,
    dislikeCount: PropTypes.number.isRequired,
  }).isRequired,
};

export default withRouter(withTheme()(Video));

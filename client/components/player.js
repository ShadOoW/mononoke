import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import Plyr from 'react-plyr';

const styles = () => ({
  root: {
    width: '100%',
  },
});

const Player = (props) => {
  const {classes} = props;
  return (
      <div className={classes.root}>
        <Plyr
            type="youtube"
            videoId={props.videoId}
        />
      </div>
  );
};

Player.propTypes = {
  classes: PropTypes.object.isRequired,
  videoId: PropTypes.string.isRequired,
};

export default withStyles(styles)(Player);

import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';

import { formatNumbers } from '../helpers';

import colors from '../styles/colors';
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import ThumbDownIcon from "@material-ui/icons/ThumbDown";

const styles = theme => (
    {
      ...colors(theme),
      ...{
        root: {
          width: '160px',
        },

        container: {
          backgroundColor: theme.palette.disabled,
        },

        text: {
          fontSize: '1.6rem',
        },

        textIconSpacing: {
          paddingLeft: '10px',
        },

        views: {
          paddingBottom: '15px',
        },

        likeBar: {
          height: '2px',
          backgroundColor: theme.palette.primary.main,
        }
      }
    }
);

const Sentiment = (props) => {
  const {classes} = props;
  const progress = {width: `${(props.likes / (props.likes + props.dislikes)) * 100}%`};

  return (
      <div className={[classes.root, 'flex', 'flex-column', 'align-items-end'].join(' ')}>
        <div className={[classes.views, classes.disabled, 'relative', 'width-100'].join(' ')}>
          <Typography color="inherit" className={[classes.text, "text-align-right"].join(' ')}>
            {formatNumbers(props.views)} Views
          </Typography>
          <div className={[classes.container, 'width-100', 'absolute', 'left-x0'].join(' ')}>
            <div className={classes.likeBar} style={progress} />
          </div>
        </div>

        <div className="flex flex-row width-100 justify-content-space-around">
          <div className="flex">
            <ThumbUpIcon color="disabled" />
            <Typography className={[classes.disabled, classes.text, classes.textIconSpacing].join(' ')}>
              {formatNumbers(props.likes)}
            </Typography>
          </div>
          <div className="flex">
            <ThumbDownIcon color="disabled" />
            <Typography className={[classes.disabled, classes.text, classes.textIconSpacing].join(' ')}>
              {formatNumbers(props.dislikes)}
            </Typography>
          </div>
        </div>
      </div>
  );
};

Sentiment.propTypes = {
  classes: PropTypes.object.isRequired,
  views: PropTypes.number.isRequired,
  likes: PropTypes.number.isRequired,
  dislikes: PropTypes.number.isRequired,
};


export default withStyles(styles)(Sentiment);

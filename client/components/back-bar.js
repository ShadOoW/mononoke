import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';

const styles = {
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

function BackBar(props) {
  const {classes} = props;

  const handleBackButton = () => {
    props.onBackButtonClicked();
  };

  return (
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Toolbar>
            <IconButton
                className={classes.menuButton}
                color="inherit"
                aria-label="Back"
                onClick={handleBackButton}
            >
              <KeyboardArrowLeftIcon />
            </IconButton>

            <Typography variant="h6" color="inherit">
              Back
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
  );
}

BackBar.propTypes = {
  classes: PropTypes.object.isRequired,
  onBackButtonClicked: PropTypes.func.isRequired,
};

export default withStyles(styles)(BackBar);

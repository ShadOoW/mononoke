import PropTypes from 'prop-types';
import React from 'react';

import Grid from '@material-ui/core/Grid';

import Video from './video';

const VideoList = (props) => {
  return (
      <div>
        {props.videos ? (
            <Grid container spacing={24} style={{padding: 24}}>
              {props.videos.map(video => (
                  <Grid key={video.videoId} item xs={12} sm={6} lg={4} xl={3}>
                    <div>
                      <Video video={video} />
                    </div>
                  </Grid>
              ))}
            </Grid>
        ) : null}
      </div>
  );
};

VideoList.propTypes = {
  videos: PropTypes.array.isRequired,
};


export default VideoList;

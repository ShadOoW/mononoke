import PropTypes from 'prop-types';
import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import DialogSelect from '../components/dialog-select';

const styles = () => ({
  appBar: {
    top: 'auto',
    bottom: '0',
  },
});

const NavBar = (props) => {
  const {classes} = props;

  return (
      <AppBar position="fixed" className={classes.appBar}>
        <ExpansionPanel defaultExpanded>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography align="right" color="primary" components="h1">Filter</Typography>
          </ExpansionPanelSummary>

          <ExpansionPanelDetails>
            <DialogSelect name='Channels' title='Filter Channels' />
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </AppBar>
  );
};

NavBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NavBar);

import PropTypes from 'prop-types';
import React from 'react';
import {withRouter} from 'next/router';
import Router from 'next/router';

import '../styles/listing.scss';

import videos from '../mocks/videos';

import VideoList from '../components/video-list';
import Detail from '../components/detail';
import BackBar from '../components/back-bar';
import NavBar from '../partials/nav-bar';

class ListPage extends React.Component {
  static async getInitialProps() {
    return {
      videos: await fetchVideos()
    };
  }

  render() {
    return (
        <React.Fragment>
          {typeof this.props.router.query.id === 'undefined' && (
              <React.Fragment>
                <NavBar />
                <VideoList videos={this.props.videos} />
              </React.Fragment>
          )}

          {this.props.router.query.id && (
              <React.Fragment>
                <BackBar onBackButtonClicked={() => Router.push('/')} />
                <Detail video={this.props.videos.find(video => video.videoId = this.props.router.query.id)} />
              </React.Fragment>
          )}
        </React.Fragment>
    );
  }
}

ListPage.propTypes = {
  router: PropTypes.object.isRequired,
  videos: PropTypes.array.isRequired,
  onBackButtonClicked: PropTypes.func.isRequired,
};

const fetchVideos = async () => {
  return videos.videos;
};

export default withRouter(ListPage);

import * as r from 'rethinkdb';

export const getConnection = async (): Promise<r.Connection> => (
    await r.connect({
      host: process.env.DB_HOST,
      port: Number(process.env.DB_PORT),
      db: process.env.DB_NAME,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
    })
);

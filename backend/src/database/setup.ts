import * as r from 'rethinkdb';

export const setup = async (conn: r.Connection): Promise<any> => {
    // await r.dbDrop('test');
    // await r.dbCreate('mononoke');

    // await r.tableCreate('channels');
    //await r.table('channels').insert([
    //    {
    //        "name": "Hespress",
    //        "id": "UCvyivffb2WwNKonKiLwVAEQ"
    //    },
    //    {
    //        "name": "Chouftv - شوف تيفي",
    //        "id": "UCI3ESb2GoE1w5MIKosbmutQ"
    //    }
    //]);

    // await r.tableCreate('youtube');

    await r.table('youtube').indexCreate('scoutedAt').run(conn);
    await r.table('youtube').indexCreate('channelId').run(conn);
};

import * as r from 'rethinkdb';

export const upsertBulk = async (ref: string, items: any, conn: r.Connection, conflict: any = 'update') => (
    await r.db('mononoke').table(ref).insert(items, {conflict}).run(conn)
);

export const update = async (ref: string, id: string, statistics: Array<any>, conn: r.Connection) => (
    await r.db('mononoke').table(ref).get(id).update(
        { statistics: r.row('statistics').append(statistics) }
    ).run(conn)
);

export const getPreviousScoutTime = async (id: string, conn: r.Connection): Promise<string> => {
    let previousScoutTime = null;

    const query = await r
        .db('mononoke')
        .table('youtube')
        .getAll(id, {index: 'channelId'})
        .orderBy(r.desc('scoutedAt'))
        .limit(1)
        .run(conn);

    const response = await query.toArray();

    if (response.length) {
        previousScoutTime = new Date(response[0].scoutedAt).toISOString();
    }

    return previousScoutTime;
};

export const getChannels = async (conn: r.Connection): Promise<Array<{id: string, name: string}>> => {
    const query = await r
        .db('mononoke')
        .table('channels')
        .run(conn);

    return await query.toArray();
};

export const getVideosSince = async (to: number, conn: r.Connection): Promise<Array<any>> => {
    const query = await r
        .db('mononoke')
        .table('youtube')
        .filter(
            r.row('scoutedAt').during(r.now().sub(to), r.now())
        )
        .run(conn);

    return await query.toArray();
};
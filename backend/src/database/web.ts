import * as r from 'rethinkdb';

export const getFromTo = async ({ size, index, to }: { size: number, index: number, to: number }, conn: r.Connection): Promise<Array<any>> => {
    const query = await r
        .db('mononoke')
        .table('youtube')
        .filter(
            r.row('scoutedAt').during(r.now().sub(to), r.now())
        )
        .orderBy(r.desc('newViews'))
        .skip(index)
        .limit(size)
        .run(conn);

    return await query.toArray();
};
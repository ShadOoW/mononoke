import { Handler } from 'aws-lambda';
import { getConnection } from '../database/connection'
import * as r from 'rethinkdb';


import { getChannels, getVideosSince, upsertBulk, getPreviousScoutTime } from '../database/admin';
import { scout, statistics } from '../api/youtube';
import { ONE_DAY } from '../config';

export const youtube: Handler = async () => {
    const conn = await getConnection();

    try {
        const channels = await getChannels(conn);

        let videos = [];
        for (const channel of channels) {
            const previousScoutTime = await getPreviousScoutTime(channel.id, conn);
            videos = videos.concat(await scout({ channel, previousScoutTime }));
        }

        const retrieveLogs = await upsertBulk('youtube', videos, conn);

        const videosToScan = await getVideosSince(ONE_DAY, conn);
        const scoutedAt = r.now();

        let data = [];
        for (const video of videosToScan) {
            data = [...data, await statistics(video, scoutedAt)];
        }

        let statisticsLogs = await upsertBulk(
            'youtube',
            data,
            conn,
            (id, oldDoc, newDoc) => {
                return r.branch(oldDoc.hasFields('statistics'),
                    oldDoc.merge({
                        statistics: oldDoc('statistics').union(newDoc('statistics'))
                    }),
                    oldDoc.merge({
                        statistics: newDoc('statistics')
                    })
                );
            }
        );

        conn.close().then();

        return { statusCode: 200, body: JSON.stringify({ retrieveLogs, statisticsLogs }) };
    } catch (error) {
        if (error instanceof Error) { console.error(error) }
        return { statusCode: 500, body: JSON.stringify(error) };
    } finally {
        if (conn) { conn.close().then() }
    }
};

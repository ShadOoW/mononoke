import { Handler } from 'aws-lambda';
import { getConnection } from '../database/connection'

import { getChannels, upsertBulk } from '../database/admin';
import { channelInfo } from '../api/youtube';

export const channels: Handler = async () => {
    const conn = await getConnection();

    try {
        const channels = await getChannels(conn);

        let info = [];
        for (const channel of channels) {
            info = info.concat(await channelInfo(channel.id));
        }

        const retrieveLogs = await upsertBulk('channels', info, conn);

        conn.close().then();

        return { statusCode: 200, body: JSON.stringify({ retrieveLogs }) };
    } catch (error) {
        if (error instanceof Error) { console.error(error) }
        return { statusCode: 500, body: JSON.stringify(error) };
    } finally {
        if (conn) { conn.close().then() }
    }
};

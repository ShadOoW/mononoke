import { Handler } from 'aws-lambda';
import * as r from 'rethinkdb';
import { getConnection } from '../database/connection'
import { getVideosSince, upsertBulk } from '../database/admin';
import { statistics } from '../api/youtube';
import { ONE_DAY } from '../config';

export const updateStatistics: Handler = async () => {
    const conn = await getConnection();

    try {
        const videos = await getVideosSince(ONE_DAY, conn);
        const scoutedAt = r.now();
        let operations = [];

        for (const video of videos) {
            operations = operations.concat(
                await upsertBulk(
                    'youtube',
                    await statistics(video, scoutedAt),
                    conn,
                    (id, oldDoc, newDoc) => {
                        return r.branch(oldDoc.hasFields('statistics'),
                            oldDoc.merge({
                                statistics: oldDoc('statistics').union(newDoc('statistics'))
                            }),
                            oldDoc.merge({
                                statistics: newDoc('statistics')
                            })
                        );
                    }
                )
            );
        }

        conn.close().then();

        return { statusCode: 200, body: JSON.stringify(operations) };
    } catch (error) {
        if (error instanceof Error) { console.error(error) }
        return { statusCode: 500, body: JSON.stringify(error) };
    } finally {
        if (conn) { conn.close().then() }
    }
};

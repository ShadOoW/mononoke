import { Handler } from 'aws-lambda';
import { getConnection } from '../database/connection'

import { getVideosSince, upsertBulk } from '../database/admin';
import { ONE_DAY } from '../config';

export const mostWatched: Handler = async () => {
    const conn = await getConnection();

    try {
        const videos = await getVideosSince(ONE_DAY, conn);
        const updatedVideos = [];

        for (const video of videos) {
            // Array should already be sorted
            // But just in case let sort it.
            // TODO: store statistics's scout time as a rethinkdb date type
            // video.statistics.sort((a, b) => ( a.scoutedAt - b.scoutedAt ));

            if (video.statistics.length > 2) {
                const first = video.statistics[video.statistics.length - 2];
                const last = video.statistics[video.statistics.length - 1];

                updatedVideos.push({
                    newViews: last.viewCount - first.viewCount,
                    videoId: video.videoId,
                });
            } else {
                updatedVideos.push({
                    newViews: -1,
                    videoId: video.videoId,
                });
            }
        }

        const logs = await upsertBulk('youtube', updatedVideos, conn);

        conn.close().then();


        return { statusCode: 200, body: JSON.stringify({ logs }) };

    } catch (error) {
        if (error instanceof Error) { console.error(error) }
        return { statusCode: 500, body: JSON.stringify(error) };
    } finally {
        if (conn) { conn.close().then() }
    }
};

import { Handler } from 'aws-lambda';
import { getConnection } from '../database/connection'
import { getChannels, upsertBulk, getPreviousScoutTime } from '../database/admin';
import { setup } from '../database/setup'
import { scout } from '../api/youtube';

export const retrieve: Handler = async () => {
    const conn = await getConnection();
    //await setup(conn);

    try {
        const channels = await getChannels(conn);

        let videos = [];
        for (const channel of channels) {
            const previousScoutTime = await getPreviousScoutTime(channel.id, conn);
            videos = videos.concat(await scout({ channel, previousScoutTime }));
        }

        const operation = await upsertBulk('youtube', videos, conn);

        conn.close().then();

        return { statusCode: 200, body: JSON.stringify(operation) };
    } catch (error) {
        if (error instanceof Error) { console.error(error) }
        return { statusCode: 500, body: JSON.stringify(error) };
    } finally {
        if (conn) { conn.close().then() }
    }
};

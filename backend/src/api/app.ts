import * as express from 'express';
import { Request, Response } from 'express';
import { json, urlencoded } from 'body-parser';
import * as cors from 'cors';
import { eventContext } from 'aws-serverless-express/middleware';
import './config';

import { WebRouter } from './routes';
import { YoutubeRouter } from './routes';

export const configureApp = () => {
    const app = express();
    app.use(cors());
    app.use(json());
    app.use(urlencoded({ extended: true }));
    app.use(eventContext());

    app.get('/api/', (request: Request, response: Response) => {
       response.send('welcome');
    });

    app.get('/api/hello', (request: Request, response: Response) => {
        response.send('Hello World!')
    });

    app.use('/api/web', WebRouter);
    app.use('/api/youtube', YoutubeRouter);

    return app;
};

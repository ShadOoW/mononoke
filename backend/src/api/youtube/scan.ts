import axios  from 'axios';
import { iso8601 } from 'rethinkdb';

import { handleError } from '../helper';
import { YT_KEY } from '../config';

export const scan = async ({ videoId, channelId, scoutedAt }) => {
    const url = 'https://www.googleapis.com/youtube/v3/videos?' +
        'id=' + videoId +
        '&key=' + YT_KEY +
        '&fields=items(snippet,status,topicDetails)&part=snippet,status,topicDetails';

    console.info('video url: ' + url);

    const resp = await axios.get(url).catch(handleError);

    // resp.data.items[0].publishedAt = iso8601(resp.data.items[0].video.snippet.publishedAt);
    // console.log(resp.data.items[0].video.snippet.publishedAt);
    // console.log(resp.data.items[0].publishedAt);

    return { videoId, channelId, scoutedAt, video: resp.data.items[0] };
};

import axios  from 'axios';

import { handleError } from '../helper';
import { YT_KEY } from '../config';

export const statistics = async ({ videoId }, scoutedAt) => {
    const url = 'https://www.googleapis.com/youtube/v3/videos?' +
        'id=' + videoId +
        '&key=' + YT_KEY +
        '&fields=items(statistics)' +
        '&part=statistics';

    console.info('video url: ' + url);

    const resp = await axios.get(url).catch(handleError);

    if (resp.data.items[0]) {
        return { videoId, statistics: [{ ...resp.data.items[0].statistics, scoutedAt }] };
    } else {
        console.info('video was probably deleted');
        return null;
    }
};

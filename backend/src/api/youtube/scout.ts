import axios from 'axios';
import { now } from 'rethinkdb';

import { scan } from './scan';
import { handleError } from '../helper';
import { YT_KEY } from '../config';

export const scout = async ({ channel, order='date', max=5, previousScoutTime = null }): Promise<any> => {
    const scoutedAt = now();

    let url = 'https://www.googleapis.com/youtube/v3/search' +
        '?key=' + YT_KEY +
        '&channelId=' + channel.id +
        '&part=id' +
        '&order=' + order +
        '&maxResults=' + max;


    if (previousScoutTime) {
        url += '&publishedAfter=' + previousScoutTime;
    }

    console.info('Channel: ' + channel.name);
    console.info('Url: ' + url);
    console.info('Scouted At: ' + scoutedAt);

    const resp = await axios.get(url).catch(handleError);
    const videoIds = resp.data.items.map((id) => ( id.id.videoId ));

    let videos = [];

    for (const videoId of videoIds) {
        videos.push(await scan({ videoId, channelId: channel.id, scoutedAt }));
    }

    return videos;
};

import axios  from 'axios';

import { handleError } from '../helper';
import { YT_KEY } from '../config';

export const channelInfo = async (id) => {
    const url = 'https://www.googleapis.com/youtube/v3/channels?' +
        'key=' + YT_KEY +
        '&id=' + id +
        '&part=snippet';

    console.info('channel url: ' + url);

    const resp = await axios.get(url).catch(handleError);

    return { id, snippet: resp.data.items[0].snippet };
};

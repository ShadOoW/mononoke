import { AxiosError } from 'axios';

export const handleError = (error: AxiosError) => {
    if (error.response) {
        throw { error: error.response.data };
    } else if (error.request) {
        throw { error: error.request };
    } else {
        throw { error: error.message };
    }
};

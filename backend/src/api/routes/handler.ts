import { Response } from 'express';

export default async (action: () => void, response: Response) => {
    try {
        response.send(await action());
    } catch (error) {
        if (error instanceof Error) {
            response.status(500).send({
                title: 'Something broke!',
                message: error.message,
                stack: error.stack,
            });
        } else {
            response.status(500).send({
                title: 'Something broke!',
                message: error,
            });
        }
    }
};

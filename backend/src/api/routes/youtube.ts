import { Router, Request, Response } from 'express';
import { scout, scan } from '../youtube';

const router: Router = Router();

router.get('/', async (request: Request, response: Response) => {
    response.send('youtube api');
});

router.post('/scout/', async (request: Request, response: Response) => {
    await exec(async () => {
        response.send(await scout(request.body));
    }, response);
});

router.get('/scan/:channelId/:videoId', async (request: Request, response: Response) => {
    await exec(async () => {
        const scoutedAt = (new Date()).toISOString();
        response.send(await scan({ videoId: request.params.videoId, channelId: request.params.channelId, scoutedAt }));
    }, response);
});

const exec = async (action: () => void, response: Response) => {
    try {
        response.send(await action());
    } catch (error) {
        if (error instanceof Error) {
            response.status(500).send({
                title: 'Something broke!',
                message: error.message,
                stack: error.stack,
            });
        } else {
            response.status(500).send({
                title: 'Something broke!',
                message: error,
            });
        }
    }
};

export const YoutubeRouter: Router = router;

import { Router, Request, Response } from 'express';
import handle from './handler';
import { getConnection } from '../../database/connection';
import { getFromTo } from '../../database/web';

const router: Router = Router();

router.get('/', async (request: Request, response: Response) => {
    response.send('web api');
});

router.get('/videos', async (request: Request, response: Response) => {
    await handle(async () => {
        response.send(await getFromTo({ size: 10, index: 0, to: 24 * 60 * 60 }, await getConnection()));
    }, response);
});

export const WebRouter: Router = router;

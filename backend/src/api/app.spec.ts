import * as chai from 'chai';
const chaiSubset = require('chai-subset');
chai.use(chaiSubset);
import * as request from 'supertest';
import { configureApp } from './app';

const expect = chai.expect;
const should = chai.should();

describe('say hello to express', () => {
    it('express say hello back to you <3', async () => {
        const app = configureApp();

        request(app)
            .get('/api/')
            .expect('Content-Type', 'text/html; charset=utf-8')
            .expect('Content-Length', '7')
            .expect(200)
            .expect(200, 'welcome')
            .end(function(err, res) {
                if (err) throw err;
            });


        request(app)
            .get('/api/hello')
            .expect('Content-Type', 'text/html; charset=utf-8')
            .expect('Content-Length', '12')
            .expect(200)
            .expect(200, 'Hello World!')
            .end(function(err, res) {
                if (err) throw err;
            });
    });
});
